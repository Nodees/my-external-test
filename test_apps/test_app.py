import unittest

from Apps.validators import Validators

validator = Validators()


class MyTestCase(unittest.TestCase):
    def test_Election_Insufficient_Age_Literate(self):
        result = validator.election('03/10/2006', '02/10/2022', 0)
        self.assertEqual('Idade insuficiente', result)

        # 2

    def test_Election_Insufficient_Age_Illiterate(self):
        result = validator.election('03/10/2006', '02/10/2022', 1)
        self.assertEqual('Idade insuficiente', result)

        # 3

    def test_Election_Facultative_Teenager_Literate(self):
        result = validator.election('02/10/2006', '02/10/2022', 0)
        self.assertEqual('Facultativo', result)
        # 5

    def test_Election_Mandatory_Literate(self):
        result = validator.election('02/10/2004', '02/10/2022', 0)
        self.assertEqual('Obrigatório', result)

        # 6

    def test_Election_Facultative_Elder_Literate(self):
        result = validator.election('02/10/1951', '02/10/2022', 0)
        self.assertEqual('Facultativo', result)

        # 7

    def test_Election_Mandatory_Elder_Literate(self):
        result = validator.election('03/10/1951', '02/10/2022', 0)
        self.assertEqual('Obrigatório', result)

        # 8

    def test_Election_Facultative_Elder_Illiterate(self):
        result = validator.election('03/10/1951', '02/10/2022', 1)
        self.assertEqual('Facultativo', result)

        # 8

    def test_Election_FacultativeElder_Illiterate(self):
        result = validator.election('03/10/1951', '02/10/2022', 1)
        self.assertEqual('Facultativo', result)

        # 9

    def test_Election_Facultative_Teenager_Illiterate(self):
        result = validator.election('02/10/2004', '02/10/2022', 1)
        self.assertEqual('Facultativo', result)

    if __name__ == '__main__':
        unittest.main()
